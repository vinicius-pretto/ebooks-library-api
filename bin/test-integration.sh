#!/bin/bash

NODE_ENV=test \
LOG_LEVEL=debug \
UPLOADS_DIR=uploads-test \
MONGODB_URI=mongodb://localhost:27017/ebooks_library
node_modules/.bin/nyc node_modules/.bin/mocha --require test/integration/helpers.js test/integration/**/*.test.js --exit
