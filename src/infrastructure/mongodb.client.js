const mongoose = require('mongoose');
const config = require('../../config');
const logger = require('../core/logger').createLogger(__filename);

class MongoDBClient {
  constructor() {
    this.config = config;
  }

  async connect() {
    try {
      await mongoose.connect(this.config.mongoDBUrl, this.config.mongoDBOptions);
      logger.info('MongoDB connection successfully');
    } catch (error) {
      logger.error('MongoDB connection failed', error);
    }
  }
}

module.exports = MongoDBClient;
