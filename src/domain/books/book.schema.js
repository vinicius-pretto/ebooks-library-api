const Joi = require('joi');

const bookSchema = Joi.object().keys({
  id: Joi.string(),
  name: Joi.string()
    .min(3)
    .max(80)
    .required(),
  description: Joi.string()
    .min(3)
    .required(),
  pictureUrl: Joi.string().required(),
  details: Joi.object().keys({
    author: Joi.string()
      .min(3)
      .max(30)
      .required(),
    language: Joi.string().required(),
    publisher: Joi.string().required(),
    year: Joi.number().required(),
    pagesNumber: Joi.number().required()
  }),
  isBorrowed: Joi.boolean(),
  borrowing: Joi.object().keys({
    userId: Joi.string(),
    borrowAt: Joi.string()
  }),
  createdAt: Joi.string(),
  updatedAt: Joi.string()
});

module.exports = bookSchema;
