const uuid = require('uuid/v4');

class BookRepository {
  constructor(BookModel) {
    this.BookModel = BookModel;
  }

  create(book) {
    const id = uuid();
    const bookToCreate = { ...book, id, borrowing: {} };
    return this.BookModel.create(bookToCreate);
  }

  findAll() {
    return this.BookModel.find({}).select('-_id -__v');
  }

  find(query) {
    return this.BookModel.find(query).select('-_id -__v');
  }

  async findById(bookId) {
    const book = await this.BookModel.where({ id: bookId })
      .findOne()
      .select('-_id -__v');

    return book || {};
  }

  update(bookId, book) {
    return this.BookModel.findOneAndUpdate({ id: bookId }, book);
  }

  remove(bookId) {
    return this.BookModel.deleteOne({ id: bookId });
  }
}

module.exports = BookRepository;
