const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
  id: {
    type: String
  },
  name: String,
  description: String,
  pictureUrl: String,
  details: {
    author: String,
    language: String,
    publisher: String,
    year: Number,
    pagesNumber: Number
  },
  isBorrowed: {
    type: Boolean,
    default: false
  },
  borrowing: {
    userId: String,
    borrowAt: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

const BookModel = mongoose.model('Book', BookSchema);

module.exports = BookModel;
