const HttpStatus = require('http-status');
const router = require('express').Router();
const BookModel = require('./book.model');
const BookRepository = require('./book.repository');
const BookController = require('./book.controller');
const UserModel = require('../users/user.model');
const UserRepository = require('../users/user.repository');
const UserController = require('../users/user.controller');
const errors = require('../../core/errors');

const bookRepository = new BookRepository(BookModel);
const bookController = new BookController(bookRepository);
const userRepository = new UserRepository(UserModel);
const userController = new UserController(userRepository);

router.post('/books', bookController.validate, async (req, res, next) => {
  try {
    const bookCreated = await bookController.create(req.body);
    res.status(HttpStatus.CREATED).json({
      id: bookCreated.id
    });
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.get('/books', async (req, res, next) => {
  try {
    const books = await bookController.findAll();
    res.json({ books });
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.get('/books/:bookId', async (req, res, next) => {
  try {
    const book = await bookController.findById(req.params.bookId);
    res.json(book);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.put('/books/:bookId/borrow', async (req, res, next) => {
  try {
    const userId = req.user.id;
    const bookId = req.params.bookId;
    const deliveryDate = await bookController.markAsBorrow(bookId, userId);
    await userController.borrowBook(userId, bookId);
    res.json(deliveryDate);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.put('/books/:bookId', async (req, res, next) => {
  try {
    await bookController.update(req.params.bookId, req.body);
    res.sendStatus(HttpStatus.OK);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.delete('/books/:bookId', async (req, res, next) => {
  try {
    await bookController.remove(req.params.bookId);
    res.sendStatus(HttpStatus.OK);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

module.exports = router;
