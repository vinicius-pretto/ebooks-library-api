/* eslint-disable class-methods-use-this */
const Joi = require('joi');
const _ = require('lodash');
const moment = require('moment');
const bookSchema = require('./book.schema');
const errors = require('../../core/errors');
const DATE_PATTERN = 'YYYY-MM-DD';

class BookController {
  constructor(bookRepository) {
    this.bookRepository = bookRepository;
  }

  async validate(req, res, next) {
    try {
      await Joi.validate(req.body, bookSchema);
      next();
    } catch (error) {
      const detail = _.first(error.details).message;
      next(errors.validationError(error, detail));
    }
  }

  create(book) {
    return this.bookRepository.create(book);
  }

  findBorrowedBooks(bookIds) {
    return this.bookRepository.find({
      id: {
        $in: bookIds
      }
    });
  }

  findAll() {
    return this.bookRepository.findAll();
  }

  findById(bookId) {
    return this.bookRepository.findById(bookId);
  }

  update(bookId, book) {
    return this.bookRepository.update(bookId, book);
  }

  async markAsBorrow(bookId, userId) {
    const book = await this.findById(bookId);
    const borrowAt = moment().format(DATE_PATTERN);
    const deliveryDate = moment(borrowAt, DATE_PATTERN)
      .add(1, 'week')
      .format(DATE_PATTERN);

    const bookToUpdate = {
      ...book.toJSON(),
      isBorrowed: true,
      borrowing: { userId, borrowAt }
    };
    await this.update(bookId, bookToUpdate);
    return Promise.resolve({ deliveryDate });
  }

  remove(bookId) {
    return this.bookRepository.remove(bookId);
  }
}

module.exports = BookController;
