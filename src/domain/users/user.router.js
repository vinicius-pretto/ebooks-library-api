const express = require('express');
const HttpStatus = require('http-status');
const _ = require('lodash');
const UserModel = require('./user.model');
const UserRepository = require('./user.repository');
const UserController = require('./user.controller');
const BookModel = require('../books/book.model');
const BookRepository = require('../books/book.repository');
const BookController = require('../books/book.controller');
const errors = require('../../core/errors');

const userRepository = new UserRepository(UserModel);
const userController = new UserController(userRepository);
const bookRepository = new BookRepository(BookModel);
const bookController = new BookController(bookRepository);
const router = express.Router();

router.post('/users', userController.validate, async (req, res, next) => {
  try {
    const userCreated = await userController.create(req.body);
    res.status(HttpStatus.CREATED).json({ id: userCreated.id });
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.get('/users', async (req, res, next) => {
  try {
    const users = await userController.findAll();
    const statusCode = _.isEmpty(users) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
    res.status(statusCode).json({ users });
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.get('/users/:userId/books', async (req, res, next) => {
  try {
    const user = await userController.findById(req.params.userId);
    const books = await bookController.findBorrowedBooks(user.borrowedBooks);
    res.json({ books });
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.get('/users/:userId', async (req, res, next) => {
  try {
    const user = await userController.findById(req.params.userId);
    const statusCode = _.isEmpty(user) ? HttpStatus.NO_CONTENT : HttpStatus.OK;
    res.status(statusCode).json(user);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.put('/users/:userId', async (req, res, next) => {
  try {
    await userController.update(req.params.userId, req.body);
    res.sendStatus(HttpStatus.OK);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

router.delete('/users/:userId', async (req, res, next) => {
  try {
    await userController.remove(req.params.userId);
    res.sendStatus(HttpStatus.OK);
  } catch (error) {
    next(errors.internalServerError(error));
  }
});

module.exports = router;
