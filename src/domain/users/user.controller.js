/* eslint-disable class-methods-use-this */
const _ = require('lodash');
const Joi = require('joi');
const errors = require('../../core/errors');

const userSchema = Joi.object().keys({
  id: Joi.string(),
  firstName: Joi.string()
    .min(3)
    .max(30)
    .required(),
  lastName: Joi.string()
    .min(3)
    .max(30)
    .required(),
  email: Joi.string().required(),
  birthDate: Joi.date(),
  telephone: Joi.string().required(),
  role: Joi.string().required(),
  borrowedBooks: Joi.array(),
  password: Joi.string()
    .required()
    .min(3)
    .max(30),
  createdAt: Joi.string(),
  updatedAt: Joi.string()
});

class UserController {
  constructor(userRepository) {
    this.userRepository = userRepository;
  }

  async validate(req, res, next) {
    try {
      await Joi.validate(req.body, userSchema);
      next();
    } catch (error) {
      const detail = _.first(error.details).message;
      next(errors.validationError(error, detail));
    }
  }

  create(user) {
    return this.userRepository.create(user);
  }

  findAll() {
    return this.userRepository.findAll();
  }

  findById(userId) {
    return this.userRepository.findById(userId);
  }

  findOne(query) {
    return this.userRepository.findOne(query);
  }

  update(userId, user) {
    return this.userRepository.update(userId, user);
  }

  remove(userId) {
    return this.userRepository.remove(userId);
  }

  async borrowBook(userId, bookId) {
    const user = await this.findById(userId);
    const borrowedBooks = [...user.borrowedBooks, bookId];
    const userToUpdate = {
      ...user,
      borrowedBooks
    };
    return this.update(userId, userToUpdate);
  }
}

module.exports = UserController;
