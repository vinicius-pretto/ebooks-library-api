const mongoose = require('mongoose');
const uuid = require('uuid/v4');
const bcrypt = require('bcrypt');
const config = require('../../../config');
const UserRole = require('./user-role');

const UserSchema = mongoose.Schema({
  id: {
    type: String
  },
  firstName: {
    type: String,
    required: true,
    minlength: [3, 'Name must have at least 3 characters'],
    maxlength: [30, 'Name must not have more than 30 characters']
  },
  lastName: {
    type: String,
    required: true,
    minlength: [3, 'Name must have at least 3 characters'],
    maxlength: [30, 'Name must not have more than 30 characters']
  },
  email: {
    type: String,
    required: true
  },
  birthDate: {
    type: Date,
    required: true
  },
  telephone: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: Object.values(UserRole),
    required: true
  },
  password: {
    type: String,
    required: true
  },
  borrowedBooks: [],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

// eslint-disable-next-line func-names
UserSchema.pre('save', async function(next) {
  const user = this;

  if (!user.isModified('password')) {
    next();
  }
  try {
    const salt = await bcrypt.genSalt(config.saltRounds);
    const hashPassword = await bcrypt.hash(user.password, salt);
    user.id = uuid();
    user.password = hashPassword;
    next();
  } catch (error) {
    next(error);
  }
});

const UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;
