const UserRole = {
  USER: 'USER',
  ADMIN: 'ADMIN'
};

module.exports = UserRole;
