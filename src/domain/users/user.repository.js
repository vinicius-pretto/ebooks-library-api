/* eslint-disable class-methods-use-this */
const bcrypt = require('bcrypt');
const config = require('../../../config');

class UserRepository {
  constructor(UserModel) {
    this.UserModel = UserModel;
  }

  async encryptPassword(password) {
    const salt = await bcrypt.genSalt(config.saltRounds);
    const hashPassword = await bcrypt.hash(password, salt);
    return hashPassword;
  }

  create(user) {
    return this.UserModel.create(user);
  }

  findAll() {
    return this.UserModel.find({}).select('-_id -__v');
  }

  async findById(userId) {
    const resultSet = await this.UserModel.where({ id: userId })
      .findOne()
      .select('-_id -__v');

    const user = resultSet ? resultSet.toJSON() : {};
    return user;
  }

  async findOne(query) {
    const user = await this.UserModel.findOne(query).select('-_id -__v');
    return user || {};
  }

  async update(userId, userToUpdate) {
    const user = await this.findById(userId);
    const userWithEncryptPassword = { ...userToUpdate, id: userId, password: user.password };
    return this.UserModel.findOneAndUpdate({ id: userId }, userWithEncryptPassword);
  }

  remove(userId) {
    return this.UserModel.deleteOne({ id: userId });
  }
}

module.exports = UserRepository;
