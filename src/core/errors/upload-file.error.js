const HttpStatus = require('http-status');
const ErrorCode = require('./error-code');

function uploadFileError({ stack }) {
  const error = new Error();
  error.code = ErrorCode.UPLOAD_FILE_ERROR;
  error.status = HttpStatus.INTERNAL_SERVER_ERROR;
  error.message = 'Internal Server Error';
  error.detail = 'Failed to upload file';

  return {
    body: error,
    stack
  };
}

module.exports = uploadFileError;
