const HttpStatus = require('http-status');
const ErrorCode = require('./error-code');

function UnauthorizedError({ stack }) {
  const error = new Error();
  error.code = ErrorCode.UNAUTHORIZED_ERROR;
  error.status = HttpStatus.UNAUTHORIZED;
  error.message = 'Unauthorized';
  error.detail = 'Invalid username or password';

  return {
    body: error,
    stack
  };
}

module.exports = UnauthorizedError;
