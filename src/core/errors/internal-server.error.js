const HttpStatus = require('http-status');
const ErrorCode = require('./error-code');

function internalServerError({ stack }) {
  const error = new Error();
  error.code = ErrorCode.INTERNAL_SERVER_ERROR;
  error.status = HttpStatus.INTERNAL_SERVER_ERROR;
  error.message = 'Internal Server Error';
  error.detail = 'Oops something went wrong';

  return {
    body: error,
    stack
  };
}

module.exports = internalServerError;
