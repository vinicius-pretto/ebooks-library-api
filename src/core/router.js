const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../swagger.json');
const config = require('../../config');
const users = require('../domain/users');
const books = require('../domain/books');
const upload = require('./upload');
const auth = require('./auth');
const Auth = require('./auth/auth');

const authorize = new Auth().authorize();

class Router {
  static registerRoutes(app) {
    app.use(`${config.apiVersion}/api-docs`, swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.use(config.apiVersion, users);
    app.use(config.apiVersion, upload);
    app.use(config.apiVersion, auth);
    app.use(config.apiVersion, authorize, books);
  }
}

module.exports = Router;
