const express = require('express');
const helmet = require('helmet');
const passport = require('passport');
const compression = require('compression');
const config = require('../../config');
const Router = require('./router');
const errorHandler = require('./error-handler');
const MongoDBClient = require('../infrastructure/mongodb.client');
const UserModel = require('../domain/users/user.model');
const UserRepository = require('../domain/users/user.repository');
const UserController = require('../domain/users/user.controller');
const cors = require('./cors');

const mongoDBClient = new MongoDBClient();
const userRepository = new UserRepository(UserModel);
const userController = new UserController(userRepository);
const app = express();
const jwtStrategy = require('../core/auth/strategy/jwt-strategy')(userController);

mongoDBClient.connect();

app.use(cors);
app.use(compression());
app.use(helmet());
app.use(express.json());
app.use(config.upload.path, express.static(config.upload.folder));
passport.use(jwtStrategy);
Router.registerRoutes(app);
app.use(errorHandler);

module.exports = app;
