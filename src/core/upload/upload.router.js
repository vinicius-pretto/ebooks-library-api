const express = require('express');
const HttpStatus = require('http-status');
const FileUploader = require('./file-uploader');
const config = require('../../../config');
const logger = require('../logger').createLogger(__filename);
const errors = require('../errors');

const router = express.Router();
const fileUploader = new FileUploader();
const isProduction = process.env.NODE_ENV === 'production';

router.post('/upload', async (req, res, next) => {
  logger.debug({ req });

  const upload = fileUploader.uploadFile('file');
  upload(req, res, error => {
    if (error) {
      return next(errors.uploadFileError(error));
    }
    if (!req.file || !req.file.path || !req.file.originalname) {
      const errorMessage = 'Must have a attached file';
      return next(errors.validationError({ stack: errorMessage }, errorMessage));
    }
    const protocol = isProduction ? 'https' : 'http';
    const host = isProduction ? req.hostname : `${req.hostname}:${config.port}`;

    return res.status(HttpStatus.CREATED).json({
      file: {
        src: `${protocol}://${host}/${req.file.path}`,
        name: req.file.originalname,
        size: req.file.size,
        mimetype: req.file.mimetype
      }
    });
  });
});

module.exports = router;
