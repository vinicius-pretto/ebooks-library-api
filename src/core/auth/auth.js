/* eslint-disable class-methods-use-this */
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../../../config');

class Auth {
  comparePassword(password, encodedPassword) {
    if (password && encodedPassword) {
      return bcrypt.compare(password, encodedPassword);
    }
    return Promise.resolve(false);
  }

  async generateToken(user) {
    const payload = {
      email: user.email,
      role: user.role
    };
    const token = await jwt.sign(payload, config.jwtSecret);
    return token;
  }

  authorize() {
    return passport.authenticate('jwt', config.jwtSession);
  }
}

module.exports = Auth;
