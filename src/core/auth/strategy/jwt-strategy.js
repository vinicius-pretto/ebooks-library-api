const { ExtractJwt, Strategy } = require('passport-jwt');
const config = require('../../../../config');

function jwtStrategy(userController) {
  const options = {
    secretOrKey: config.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
  };

  const strategy = new Strategy(options, async (payload, done) => {
    try {
      const user = await userController.findOne({ email: payload.email });

      if (user) {
        done(null, {
          id: user.id,
          email: user.email,
          role: user.role
        });
      } else {
        done(null, false);
      }
    } catch (error) {
      done(error, false);
    }
  });
  return strategy;
}

module.exports = jwtStrategy;
