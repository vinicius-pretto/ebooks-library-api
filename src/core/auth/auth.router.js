const express = require('express');
const errors = require('../errors');
const UserModel = require('../../domain/users/user.model');
const UserRepository = require('../../domain/users/user.repository');
const UserController = require('../../domain/users/user.controller');
const Auth = require('./auth');

const router = express.Router();
const userRepository = new UserRepository(UserModel);
const userController = new UserController(userRepository);
const auth = new Auth();

router.post('/auth', async (req, res, next) => {
  const { username, password } = req.body;
  const error = { stack: 'Unauthorized' };

  if (username && password) {
    const user = await userController.findOne({ email: username });
    const encodedPassword = user.password;
    const isAuthorized = await auth.comparePassword(password, encodedPassword);

    if (isAuthorized) {
      const token = await auth.generateToken(user);
      return res.json({ user, token });
    }
    return next(errors.unauthorizedError(error));
  }
  return next(errors.unauthorizedError(error));
});

module.exports = router;
