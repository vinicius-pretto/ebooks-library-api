const bunyan = require('bunyan');
const bunyanFormatter = require('bunyan-format');
const path = require('path');
const config = require('../../config');

class Logger {
  static requestSerializer(req) {
    return {
      method: req.method,
      url: req.url,
      headers: req.headers,
      params: req.params,
      query: req.query
    };
  }

  static errorSerializer(error) {
    return {
      title: error.title,
      message: error.message,
      stack: error.stack
    };
  }

  static createLogger(name) {
    const log = bunyan.createLogger({
      name: path.basename(name),
      level: config.logLevel,
      stream: bunyanFormatter({
        outputMode: 'short',
        levelInString: true
      }),
      serializers: {
        req: this.requestSerializer,
        error: this.errorSerializer
      }
    });
    return log;
  }
}

module.exports = Logger;
