# Ebooks Library API

## Requiments

- [Node.js](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)
- [MongoDB](https://www.mongodb.com/)

## Running Application

### Install Dependencies

```
$ npm install
```

### Start MongoDB

You can download and start MongoDB service here:

- [MongoDB](https://www.mongodb.com/download-center)

Or you can start a [MongoDB](https://hub.docker.com/_/mongo) [Docker container](https://www.docker.com/) running this command:

```
$ npm run db:start
```

### Start Application

```
$ npm start
```

## Run Tests

```
$ npm test
```

## [Swagger (API docs)](https://swagger.io/)

```
http://localhost:8080/v1/api-docs
```
