const defaultUser = require('../mocks/user.json');

class User {
  constructor({
    id = defaultUser.id,
    firstName = defaultUser.firstName,
    lastName = defaultUser.lastName,
    email = defaultUser.email,
    birthDate = defaultUser.birthDate,
    telephone = defaultUser.telephone,
    role = defaultUser.role,
    borrowedBooks = defaultUser.borrowedBooks,
    password = defaultUser.password,
    createdAt = defaultUser.createdAt,
    updatedAt = defaultUser.updatedAt
  } = {}) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.birthDate = birthDate;
    this.telephone = telephone;
    this.role = role;
    this.borrowedBooks = borrowedBooks;
    this.password = password;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}

module.exports = User;
