const defaultBook = require('../mocks/book.json');

class Book {
  constructor(bookPayload) {
    const book = { ...defaultBook, ...bookPayload };
    if (book.borrowing) {
      this.borrowing = book.borrowing;
    }
    this.id = book.id;
    this.name = book.name;
    this.description = book.description;
    this.pictureUrl = book.pictureUrl;
    this.details = {
      author: book.details.author,
      language: book.details.language,
      publisher: book.details.publisher,
      year: book.details.year,
      pagesNumber: book.details.pagesNumber
    };
    this.isBorrowed = book.isBorrowed;
    this.createdAt = book.createdAt;
    this.updatedAt = book.updatedAt;
  }
}

module.exports = Book;
