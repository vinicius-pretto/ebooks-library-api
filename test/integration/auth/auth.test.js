/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
const HttpStatus = require('http-status');
const User = require('../../helpers/user');
const ErrorCode = require('../../../src/core/errors/error-code');

describe('Auth', () => {
  context('POST /v1/auth', () => {
    const defaultUser = new User();

    beforeEach(async () => {
      await UserModel.deleteMany({});
      await UserModel.create(defaultUser);
    });

    context('Authentication successfully', () => {
      it('should return a jwt token', async () => {
        const credentials = {
          username: defaultUser.email,
          password: defaultUser.password
        };
        const response = await request.post('/v1/auth').send(credentials);
        expect(response.status).to.be.eql(HttpStatus.OK);
        expect(response.body.token).to.not.be.empty;
      });
    });

    context('Authentication failed', () => {
      context('Invalid username', () => {
        it('should return unauthorized', async () => {
          const credentials = {
            username: 'INVALID_USERNAME',
            password: defaultUser.password
          };

          const expectedError = {
            code: ErrorCode.UNAUTHORIZED_ERROR,
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized',
            detail: 'Invalid username or password'
          };
          const response = await request.post('/v1/auth').send(credentials);
          expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
          expect(response.body.error).to.be.eql(expectedError);
        });
      });

      context('Invalid password', () => {
        it('should return unauthorized', async () => {
          const credentials = {
            username: defaultUser.username,
            password: 'INVALID_PASSWORD'
          };

          const expectedError = {
            code: ErrorCode.UNAUTHORIZED_ERROR,
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized',
            detail: 'Invalid username or password'
          };
          const response = await request.post('/v1/auth').send(credentials);
          expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
          expect(response.body.error).to.be.eql(expectedError);
        });
      });
    });
  });
});
