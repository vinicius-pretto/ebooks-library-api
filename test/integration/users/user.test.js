/* eslint-disable no-undef */
const HttpStatus = require('http-status');
const User = require('../../helpers/user');
const Book = require('../../helpers/book');
const Auth = require('../../../src/core/auth/auth');

describe('Users', () => {
  const UUID_SIZE = 36;
  const defaultUser = new User();
  const defaultBook = new Book();
  const auth = new Auth();

  beforeEach(async () => {
    await UserModel.deleteMany({});
  });

  describe('POST /v1/users', () => {
    it('should create a new user', async () => {
      const response = await request.post('/v1/users').send(defaultUser);
      expect(response.status).to.be.eql(HttpStatus.CREATED);
      expect(response.body)
        .to.have.property('id')
        .with.lengthOf(UUID_SIZE);
    });
  });

  describe('GET /v1/users', () => {
    let userCreated;

    beforeEach(async () => {
      userCreated = await UserModel.create(defaultUser);
    });

    it('should return all users', async () => {
      const expectedUser = new User({ password: userCreated.password });
      const response = await request.get('/v1/users');
      expectedUser.id = userCreated.id;
      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(response.body.users).to.be.eql([expectedUser]);
    });
  });

  describe('GET /v1/users/:userId', () => {
    let userCreated;

    beforeEach(async () => {
      userCreated = await UserModel.create(defaultUser);
    });

    it('should return the user by id', async () => {
      const expectedUser = new User({ id: userCreated.id, password: userCreated.password });
      const response = await request.get(`/v1/users/${userCreated.id}`);
      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(response.body).to.be.eql(expectedUser);
    });
  });

  describe('GET /v1/users/:userId/books', () => {
    let token;
    let cleanCodeBook;
    let userCreated;

    beforeEach(async () => {
      cleanCodeBook = new Book({ id: '95114762-65f2-4ec4-b7e3-1cdecb33e09a', name: 'Clean Code' });
      await BookModel.deleteMany({});
      userCreated = await UserModel.create(defaultUser);
      await BookModel.create(defaultBook);
      await BookModel.create(cleanCodeBook);
      token = await auth.generateToken(defaultUser);
    });

    it('should return all borrowed books from user', async () => {
      await request
        .put(`/v1/books/${defaultBook.id}/borrow`)
        .set('Authorization', `Bearer ${token}`);

      await request
        .put(`/v1/books/${cleanCodeBook.id}/borrow`)
        .set('Authorization', `Bearer ${token}`);

      const booksResponse = await request.get('/v1/books').set('Authorization', `Bearer ${token}`);

      const borrowedBooksResponse = await request.get(`/v1/users/${userCreated.id}/books`);
      expect(borrowedBooksResponse.status).to.be.eql(HttpStatus.OK);
      expect(borrowedBooksResponse.body).to.be.eql(booksResponse.body);
    });
  });

  describe('PUT /v1/users/:userId', () => {
    let userCreated;

    beforeEach(async () => {
      userCreated = await UserModel.create(defaultUser);
    });

    it('should update the user', async () => {
      const updatedUser = new User({
        firstName: 'Joao',
        lastName: 'da Silva',
        email: 'joao@example.com',
        birthDate: '1989-05-18T00:00:00.000Z',
        telephone: '51911111111',
        role: 'ADMIN'
      });
      const response = await request.put(`/v1/users/${userCreated.id}`).send(updatedUser);
      const userResponse = await request.get(`/v1/users/${userCreated.id}`);
      updatedUser.id = userCreated.id;
      updatedUser.password = userResponse.body.password;

      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(userResponse.body).to.be.eql(updatedUser);
    });
  });

  describe('DELETE /v1/users/:userId', () => {
    let userCreated;

    beforeEach(async () => {
      userCreated = await UserModel.create(defaultUser);
    });

    it('should delete the user', async () => {
      const response = await request.delete(`/v1/users/${userCreated.id}`);
      const userResponse = await request.get(`/v1/users/${userCreated.id}`);
      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(userResponse.status).to.be.eql(HttpStatus.NO_CONTENT);
      expect(userResponse.body).to.be.eql({});
    });
  });
});
