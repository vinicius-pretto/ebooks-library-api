/* eslint-disable no-undef */
const HttpStatus = require('http-status');
const moment = require('moment');
const Book = require('../../helpers/book');
const User = require('../../helpers/user');
const Auth = require('../../../src/core/auth/auth');

describe('Books', () => {
  const UUID_SIZE = 36;
  const BOOK_ID = '85114762-65f2-4ec4-b7e3-1cdecb33d08a';
  const defaultBook = new Book();
  const defaultUser = new User();
  const auth = new Auth();
  let token;
  let userCreated;

  beforeEach(async () => {
    await UserModel.deleteMany({});
    await BookModel.deleteMany({});
    userCreated = await UserModel.create(defaultUser);
    token = await auth.generateToken(defaultUser);
  });

  context('POST /v1/books', () => {
    context('Unauthorized', () => {
      it('should return unauthorized', async () => {
        const newBook = new Book();
        const response = await request.post('/v1/books').send(newBook);
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it('should create a new book', async () => {
        const newBook = new Book();
        const response = await request
          .post('/v1/books')
          .set('Authorization', `Bearer ${token}`)
          .send(newBook);

        expect(response.status).to.be.eql(HttpStatus.CREATED);
        expect(response.body)
          .to.have.property('id')
          .with.lengthOf(UUID_SIZE);
      });
    });
  });

  context('GET /v1/books', () => {
    beforeEach(async () => {
      await BookModel.create(defaultBook);
    });

    context('Unauthorized', () => {
      it('should return unauthorized', async () => {
        const response = await request.get('/v1/books');
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it('should return all books', async () => {
        const response = await request.get('/v1/books').set('Authorization', `Bearer ${token}`);
        expect(response.status).to.be.eql(HttpStatus.OK);
        expect(response.body.books).to.be.eql([defaultBook]);
      });
    });
  });

  context('GET /v1/books/:bookId', () => {
    beforeEach(async () => {
      await BookModel.create(defaultBook);
    });

    context('Uauthorized', () => {
      it('should return unauthorized', async () => {
        const response = await request.get(`/v1/books/${BOOK_ID}`);
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it(`should get the book with id equals ${BOOK_ID}`, async () => {
        const response = await request
          .get(`/v1/books/${BOOK_ID}`)
          .set('Authorization', `Bearer ${token}`);

        expect(response.status).to.be.eql(HttpStatus.OK);
        expect(response.body).to.be.eql(defaultBook);
      });
    });
  });

  context('PUT /v1/books/:bookId', () => {
    beforeEach(async () => {
      await BookModel.create(defaultBook);
    });

    const updatedBook = new Book({
      name: 'Javascript book',
      description: 'new description',
      details: {
        author: 'Vinicius',
        language: 'Portuguese',
        publisher: 'Publisher',
        year: 2019,
        pagesNumber: 200
      }
    });

    context('Unauthorized', () => {
      it('should return unauthorized', async () => {
        const response = await request.put(`/v1/books/${BOOK_ID}`).send(updatedBook);
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it(`should update the book with id equals ${BOOK_ID}`, async () => {
        const response = await request
          .put(`/v1/books/${BOOK_ID}`)
          .set('Authorization', `Bearer ${token}`)
          .send(updatedBook);

        const bookResponse = await request
          .get(`/v1/books/${BOOK_ID}`)
          .set('Authorization', `Bearer ${token}`);

        expect(response.status).to.be.eql(HttpStatus.OK);
        expect(bookResponse.body).to.be.eql(updatedBook);
      });
    });
  });

  context('DELETE /v1/books/:bookId', () => {
    beforeEach(async () => {
      await BookModel.create(defaultBook);
    });

    context('Unauthorized', () => {
      it('should return uanauthorized', async () => {
        const response = await request.delete(`/v1/books/${BOOK_ID}`);
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it(`should delete the book with id equals ${BOOK_ID}`, async () => {
        const response = await request
          .delete(`/v1/books/${BOOK_ID}`)
          .set('Authorization', `Bearer ${token}`);

        const bookResponse = await request.get(`/v1/books/${BOOK_ID}`);

        expect(response.status).to.be.eql(HttpStatus.OK);
        expect(bookResponse.body).to.be.eql({});
      });
    });
  });

  context('PUT /v1/books/:bookId/borrow', () => {
    beforeEach(async () => {
      await BookModel.create(defaultBook);
    });

    context('Unauthorized', () => {
      it('should return uanauthorized', async () => {
        const response = await request.put(`/v1/books/${BOOK_ID}/borrow`);
        expect(response.status).to.be.eql(HttpStatus.UNAUTHORIZED);
      });
    });

    context('Authorized', () => {
      it('should borrow an existing book', async () => {
        const borrowBookResponse = await request
          .put(`/v1/books/${BOOK_ID}/borrow`)
          .set('Authorization', `Bearer ${token}`);

        const bookResponse = await request
          .get(`/v1/books/${BOOK_ID}`)
          .set('Authorization', `Bearer ${token}`);

        const userResponse = await request.get(`/v1/users/${userCreated.id}`);
        const borrowAt = bookResponse.body.borrowing.borrowAt;
        const expectedBook = new Book({
          isBorrowed: true,
          borrowing: {
            userId: userCreated.id,
            borrowAt: borrowAt
          }
        });
        const expectedDeliveryDate = moment(borrowAt, 'YYYY-MM-DD')
          .add(1, 'week')
          .format('YYYY-MM-DD');

        expect(borrowBookResponse.status).to.be.eql(HttpStatus.OK);
        expect(borrowBookResponse.body.deliveryDate).to.be.eql(expectedDeliveryDate);
        expect(bookResponse.body).to.be.eql(expectedBook);
        expect(userResponse.body.borrowedBooks).to.eql([BOOK_ID]);
      });
    });
  });
});
