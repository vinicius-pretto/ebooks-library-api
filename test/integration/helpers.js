const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../src/core/app');
const UserModel = require('../../src/domain/users/user.model');
const BookModel = require('../../src/domain/books/book.model');

chai.use(chaiHttp);

global.request = chai.request(app).keepOpen();
global.expect = chai.expect;
global.UserModel = UserModel;
global.BookModel = BookModel;
