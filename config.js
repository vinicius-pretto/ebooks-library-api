const config = {};

// Server port
config.port = process.env.PORT || 8080;

// Bunnyan log level
config.logLevel = process.env.LOG_LEVEL || 'info';

// API version
config.apiVersion = '/v1';

// Uploads
config.upload = {
  folder: process.env.UPLOADS_DIR || 'uploads',
  path: process.env.UPLOADS_PATH || '/uploads'
};

// MongoDB
config.mongoDBUrl = process.env.MONGODB_URI || 'mongodb://localhost:27017/ebooks_library';
config.mongoDBOptions = {
  useNewUrlParser: true,
  useFindAndModify: false
};

// Bcrypt
config.saltRounds = 10;

// JWT
config.jwtSecret = 'My$ecr3t';
config.jwtSession = {
  session: false
};

// Cors
config.cors = {
  allowedDomains: [
    'http://localhost:3000',
    'http://localhost:5000',
    'https://ebooks-library.herokuapp.com'
  ],
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Authorization',
    'X-Requested-With'
  ],
  allowedMethods: ['POST', 'GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS']
};

module.exports = config;
